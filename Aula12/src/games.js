import React from 'react'
import Axios from 'axios'


// estados
// encapsulamento de entrada

export default class Games extends React.Component{

    constructor(props){
        super(props)

        this.state = {
            games: []
        }
    }

    componentDidMount = () =>{
        this.handleGet()
    }

    handleGet = () =>{
        const url = 'http://localhost:3001/api/games'
        var request = Axios.get(url)
        request.then(function(response){
            console.log(response)
        })
    }

    render(){
        return <div>
            {this.state.games.map((item) =>{
                return item.nome
            })}
        </div>
    }
}