const express = require("express")
const app = express();
const oficina = require("./api/routers/oficina")

app.use(express.json())

app.use("/oficina", oficina)

var port = "9090";
app.listen(port,()=>{
    console.log("Aberto na porta: "+port)
})